// Copyright Jinseok Seo 2018.

#include "Grabber2.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Components/InputComponent.h"
#include "DrawDebugHelpers.h"

#define OUT

// Sets default values for this component's properties
UGrabber2::UGrabber2()
{
    PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber2::BeginPlay()
{
    Super::BeginPlay();
    FindPhysicsHandleComponent();
    SetupInputComponent();
}

void UGrabber2::FindPhysicsHandleComponent()
{
    PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
    if (PhysicsHandle == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("%s mssing physics handle component"), *GetOwner()->GetName());
    }
}

void UGrabber2::SetupInputComponent()
{
    InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
    if (InputComponent) {
        InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber2::Grab);
        InputComponent->BindAction("Grab", IE_Released, this, &UGrabber2::Release);
    }
    else {
        UE_LOG(LogTemp, Error, TEXT("%s mssing input component"), *GetOwner()->GetName());
    }
}

/// Called every frame
void UGrabber2::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    if (!PhysicsHandle) return;
    
    if (PhysicsHandle->GrabbedComponent) {
        PhysicsHandle->SetTargetLocation(GetLineTracePoints().v2);
    }
}

FHitResult UGrabber2::GetFirstPhysicsBodyInReach() const
{
    FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());
    FHitResult HitResult;

    FTwoVectors TracePoints = GetLineTracePoints();

    GetWorld()->LineTraceSingleByObjectType(
        OUT HitResult,
        TracePoints.v1,
        TracePoints.v2,
        FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
        TraceParameters
    );
    return HitResult;
}

void UGrabber2::Grab()
{
    auto HitResult = GetFirstPhysicsBodyInReach();
    auto ComponentToGrab = HitResult.GetComponent();
    auto ActorHit = HitResult.GetActor();

    if (ActorHit) {
        PhysicsHandle->GrabComponent(ComponentToGrab,
            NAME_None,
            ComponentToGrab->GetOwner()->GetActorLocation(),
            true);
    }
}

void UGrabber2::Release()
{
    PhysicsHandle->ReleaseComponent();
}

FTwoVectors UGrabber2::GetLineTracePoints() const
{
    FVector StartLocation;
    FRotator PlayerViewPointRotation;
    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
        OUT StartLocation, OUT PlayerViewPointRotation);

    FVector EndLocation = StartLocation +
        PlayerViewPointRotation.Vector() * Reach;
    return FTwoVectors(StartLocation, EndLocation);
}
